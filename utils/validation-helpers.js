

function isValidInt(number){
    return /^\d+$/i.test(number);
}

function isValidVehicleNumber(number){
    return /^\w{2}-\d{2}-\w{2}-\d{4}$/i.test(number);
}



module.exports = {
    isValidInt,isValidVehicleNumber
}