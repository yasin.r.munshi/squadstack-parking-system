const {isValidVehicleNumber,isValidInt} = require("./validation-helpers");

const CONFIGURATIONS = {
    /**
     * Regex for Split command and arguments
     */
    COMMAND_SPLIT: /\s+/i,

    /**
     * Command Constants and validations for quick access
     */
    COMMAND_CONSTANTS : {

        CREATE_SYSTEM: {
            name: "Create_parking_lot",
            validation(parameters){
                // Test parameter 0 is number
                if(parameters.length === 0){
                    throw new Error("Please pass the number of available slots as first parameters")
                }
                if(!isValidInt(parameters[0])){
                    throw new Error("Please pass number in as first parameters")
                }
            }


        },

        PARK: {
            name: "Park",
            validation(parameters){
                if(parameters.length < 3){
                    throw new Error("Missing arguments!")
                }
                if(!isValidVehicleNumber(parameters[0])){
                    throw new Error("Vehicle number is invalid!");
                }
                if(!isValidInt(parameters[2])){
                    throw new Error("Age is invalid!");
                }
            }
        },
        SLOT_FOR_AGE: {
            name : "Slot_numbers_for_driver_of_age",
            validation(parameters){
                if(parameters.length === 0){
                    throw new Error("Missing arguments!");
                }
                if(!isValidInt(parameters[0])){
                    throw new Error("Age is invalid!");
                }
            }
        },
        SLOT_FOR_VEHICLE :{
            name :  "Slot_number_for_car_with_number",
            validation(parameters){
                if(parameters.length === 0){
                    throw new Error("Missing arguments!");
                }
                if(!isValidVehicleNumber(parameters[0])){
                    throw new Error("Vehicle number is invalid!");
                }
            }

        },
        LEAVE : {
            name : "Leave",
            validation(parameters){
                if(parameters.length === 0){
                    throw new Error("Missing arguments!");
                }
                if(!isValidInt(parameters[0])){
                    throw new Error("Slot number is invalid!");
                }
            }
        },
        VEHICLE_FOR_AGE : {
            name : "Vehicle_registration_number_for_driver_of_age",
            validation(parameters){
                if(parameters.length === 0){
                    throw new Error("Missing arguments!");
                }
                if(!isValidInt(parameters[0])){
                    throw new Error("Age is invalid!");
                }
            }
        },

    }

}


module.exports = CONFIGURATIONS;