const {COMMAND_SPLIT,COMMAND_CONSTANTS} = require("./configuration");
const {ParkingSystem} = require("./parking-system")

class CommandManager{
    constructor() {
        this.parkingSystem = null;
    }

    /**
     * Process row command
     * @param command
     * @returns {string}
     */
    processCommand(command){
        let allParams = this.splitParameters(command);
        let commandName = allParams.shift();
        return this.handleCommand(commandName,allParams)
    }

    /**
     * Apply validation and call appropriate functions and return the response
     * @param commandName
     * @param parameters
     * @returns {string|*}
     */
    handleCommand(commandName,parameters){
        let vehicleNumber,age,slot;
        switch (commandName){
            // Create System Command
            case COMMAND_CONSTANTS.CREATE_SYSTEM.name:
                COMMAND_CONSTANTS.CREATE_SYSTEM.validation(parameters);
                this.notShouldNotInitialized();
                this.parkingSystem = new ParkingSystem(parameters[0])
                return (`Created parking of ${parameters[0]} slots`)

            // Create Park command
            case COMMAND_CONSTANTS.PARK.name:
                this.requiredParkSystem();
                COMMAND_CONSTANTS.PARK.validation(parameters);
                [vehicleNumber,age] = [parameters[0],parameters[2]]
                slot = this.parkingSystem.park(vehicleNumber,age);
                return (`Car with vehicle registration number "${vehicleNumber}" has been parked at slot number ${slot}`)

            // Slot for Age Command
            case COMMAND_CONSTANTS.SLOT_FOR_AGE.name:
                this.requiredParkSystem();
                COMMAND_CONSTANTS.SLOT_FOR_AGE.validation(parameters);
                let slots = this.parkingSystem.getSlotNumberForAge(parameters[0]);
                if(slots.length === 0){
                    throw new Error(`No parked car matches the query`);
                }
                return (slots.join(","))

            // Leave Command
            case COMMAND_CONSTANTS.LEAVE.name:
                this.requiredParkSystem();
                COMMAND_CONSTANTS.LEAVE.validation(parameters);
                let currentSlot = parameters[0];
                let vehicleInformation  = this.parkingSystem.leave(currentSlot);
                return (`Slot number ${currentSlot} vacated, the car with the vehicle registration number "${vehicleInformation.vehicleNumber}" left the space, the driver of the car was age of ${vehicleInformation.age}`)

            // Slot For Vehicle Command
            case COMMAND_CONSTANTS.SLOT_FOR_VEHICLE.name:
                this.requiredParkSystem();
                COMMAND_CONSTANTS.SLOT_FOR_VEHICLE.validation(parameters);
                slot = this.parkingSystem.getSlotForVehicleNumber(parameters[0]);
                return (slot)

            // Vehicle for Age Command
            case COMMAND_CONSTANTS.VEHICLE_FOR_AGE.name:
                this.requiredParkSystem();
                COMMAND_CONSTANTS.VEHICLE_FOR_AGE.validation(parameters);
                let vehicleNumbers  = this.parkingSystem.getVehicleNumbersForAge(parameters[0]);
                return (`Vehicle registration number(s) ${vehicleNumbers.join(",")} for age ${parameters[0]}`)
        }
        // if everything command not found
        throw new Error("Invalid Command!!");
    }

    /**
     * Check if parking system is not initialized throw error
     */
    requiredParkSystem(){
        if(!this.parkingSystem){
            throw new Error("Please call 'Create_parking_lot {availableSlot}' first to initialize system");
        }
    }

    /**
     * Check if parking system is initialized then throw error
     */
    notShouldNotInitialized(){
        if(!!this.parkingSystem){
            throw new Error("Parking lot is already created!");
        }
    }

    /**
     * Convert full command into command and argument
     * @param command
     * @returns {*|string[]}
     */
    splitParameters(command){
        return command.split(COMMAND_SPLIT);
    }

}

module.exports = {
    CommandManager
}

