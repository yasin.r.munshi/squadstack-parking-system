
class ParkingSystem{

    /**
     *
     * @param totalSlot number
     */
    constructor(totalSlot) {
        this.totalSlot = parseInt(totalSlot);

        this.availableStots = new Array(this.totalSlot)
            .fill(1)
            .map((value, index) => index+1); // Initially Create all vacant spot


        // Parking Logs for reports for future addition
        this.logs = [];
        this.activeSlots = [];


    }


    /**
     * Park Vehicle with number and age
     * @param vehicleNumber
     * @param age
     * @returns {unknown}
     */
    park(vehicleNumber,age) {
        // Check if slots are available
        if (this.availableStots.length === 0) {
            throw new Error("Sorry, parking space is full right now");
        }

        // Move latest available
        let slot = this.availableStots.shift();

        // Add Entry into logs
        this.logs.push({
            vehicleNumber,age,slot
        })


        // Add Entry in current Active Slots
        this.activeSlots.push({
            vehicleNumber,
            age,
            // Log Index to mark them un-parked  cars
            logIndex: this.logs.length - 1,
            slot
        })

        return slot;
    }


    /**
     * un-assign the current slot
     * @param slotNumber
     * @Retrun {vehicleNumber:number,age:number}
     */
    leave(slotNumber){
        // Check if slot is not assigned
        let index = this.activeSlots
                        .findIndex(parkedVehicle => parkedVehicle.slot === parseInt(slotNumber)); // Find from active slot
        if(index === -1){
            throw new Error("Slot already vacant");
        }

        let currentVachelInfo = this.activeSlots.splice(index,1)[0];

        // Re-add the slot
        this.addParkingSlot(slotNumber);

        return currentVachelInfo;
    }


    /**
     * Give slots for given driver's age
     * @param age
     * @returns {any[]}
     */
    getSlotNumberForAge(age){
        return this.activeSlots
            // Filter based on age
            .filter(parkedVehicle => parkedVehicle.age === age)
            // Map it to slot values
            .map(parkedVehicle => parkedVehicle.slot);
    }


    /**
     * return slot for given vehicle number
     * @param vehicleNumber
     * @returns {*}
     */
    getSlotForVehicleNumber(vehicleNumber){
        // Check if vehicle number is parked here or not
        let logs = this.activeSlots.filter(parkedVehicle => parkedVehicle.vehicleNumber === vehicleNumber);

        if(logs.length === 0){
            throw new Error(`No parked car matches the query`);
        }

        return logs[0].slot;
    }



    /**
     * return vehicle numbers for given drivers age
     * @param age
     * @returns {*[]}
     */
    getVehicleNumbersForAge(age){

        let allVehicles = this.activeSlots
            // find the parked car with specific age
            .filter(parkedVehicle=> parkedVehicle.age === age)
            // Map it to only vehicle number
            .map(parkedVehicle => parkedVehicle.vehicleNumber);

        if(allVehicles.length === 0){
            throw new Error(`No parked car matches the query`);
        }

        return allVehicles;
    }


    /**
     * Add Parkign Slot back
     * @param slotNumber
     */
    addParkingSlot(slotNumber){
        for(let i = 0; i <= this.availableStots.length ; i++){
            if(slotNumber < this.availableStots[i]){
                this.availableStots.splice(i,0,slotNumber);
                break;
            }
        }
    }




}


module.exports = {
    ParkingSystem
}