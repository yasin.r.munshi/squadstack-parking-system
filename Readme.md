# SquadStack Parking System 

Parking lot system with Node.js with OOP and Functional programming approach    


## Setup

### Software requirements 
 
#### Node.js version (11.x+)
- Guide to install Node.js (For Major Operating System)
   - [Install in windows](https://phoenixnap.com/kb/install-node-js-npm-on-windows)
   - [Install in Linux](https://www.geeksforgeeks.org/installation-of-node-js-on-linux/)
   - [Install in MAC](https://gist.github.com/tomysmile/da3cb8194ec8f0e4df86)

### Project setup 
You need to run following command to install all the dependency 
```
npm install
```
---
## Project Details  
### Directory Structure
> Following is directory structure for project 
```
.
+-- coverage/ (Coverage Report) 
+-- input/  (Test Input files)
+-- test/ (Unit Test Coverage)
+-- utils/ (Utility Code)
|   +-- command-manager.js (Handel Command Parsing and exiecution)
|   +-- configuration.js (Handel Configurations like command name common regex)
|   +-- parking-system.js (Handels whole parking system logic)
|   +-- validation-helpers.js (Validation Helpers)
+-- main.js (Point of execution)
```
### Dependency Details 

Here are some project dependency (this will not include the inbuilt node.js packages) 
- **mocha :** Unit testing library for nodejs
- **nyc :** For Coverage Report generations 

---
## Running Code  
To Run application please use following command
```
npm start [filename]
```
---
## UnitTest
### Running the unit test
```commandline
npm test
```
### Running with code coverage
```commandline
npm run test:coverage 
```

