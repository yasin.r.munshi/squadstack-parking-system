const assert = require('assert');
const {ParkingSystem} = require("../utils/parking-system");

describe("Parking System", ()=>{

    describe("#park()",() => {
        let parkSystem = new ParkingSystem(10);
        beforeEach(()=>{
            parkSystem = new ParkingSystem(6);
            parkSystem.park("KA-01-HH-1234",18);
            parkSystem.park("KA-02-HH-1234",20);
            parkSystem.park("KA-03-HH-1234",21);

        })


        it("it should park the car at slot 4",()=>{
            let slot = parkSystem.park("KA-01-HH-1234",18);
            assert.equal(slot,4,"Check assigned number");
        });



       it("it should park the car at slot 5",()=>{
            let slot = parkSystem.park("KA-01-HH-1234",50);
            assert.equal(slot,4,"Check assigned number");
        });
       it("it should park the car at slot 2",()=>{
            parkSystem.leave(2);
            let slot = parkSystem.park("KA-03-HH-1234",18);
            assert.equal(slot,2,"Check assigned number");
        });
       it("it should throw an error",()=>{
           try{
               parkSystem.park("KA-01-HH-1234",18);
               parkSystem.park("KA-01-HH-1234",18);
               parkSystem.park("KA-01-HH-1234",18);
               parkSystem.park("KA-01-HH-1234",18);
               parkSystem.park("KA-01-HH-1234",18);
               parkSystem.park("KA-01-HH-1234",18);
               assert.fail("Error should be thrown...");
           }catch (e){}
        });
    })

    describe("#leave()",()=>{

        let parkSystem = new ParkingSystem(100);
        beforeEach(()=>{
            parkSystem = new ParkingSystem(6);
            parkSystem.park("KA-01-HH-1234",18);
            parkSystem.park("KA-02-HH-1234",20);
            parkSystem.park("KA-03-HH-1234",21);

        })
        it("it should leave the car lot 2",()=>{
            let vehicle = parkSystem.leave(2);
            assert.equal(vehicle.vehicleNumber,"KA-02-HH-1234","Vehicle number match");
            assert.equal(vehicle.age,20,"Vehicle number match");
        });

        it("it should leave the car lot 3",()=>{
            let vehicle = parkSystem.leave(3);
            assert.equal(vehicle.vehicleNumber,"KA-03-HH-1234","Vehicle number match");
            assert.equal(vehicle.age,21,"Vehicle number match");
        });

        it("it should throw an error",()=>{
            try{
                let vehicle = parkSystem.leave(3);
                assert.fail("it should send error");
            }catch (e){}
        });
    });


    describe("#getSlotNumberForAge()",()=>{
        let parkSystem = new ParkingSystem(10);
        it("it should give slot number 2",()=>{
            parkSystem.park("KA-01-HH-1234",18);
            parkSystem.park("KA-01-HH-1234",20);
            parkSystem.park("KA-01-HH-1234",18);
            let foundSlots = parkSystem.getSlotNumberForAge(20);
            assert.equal(foundSlots.length,1,"Slot Number length 1");
            assert.equal(foundSlots[0],2,"First Element should be 2");
        })
    })

    describe("#getVehicleNumbersForAge()",()=>{
        let parkSystem = new ParkingSystem(10);
        it("it should give slot number 2",()=>{
            parkSystem.park("KA-01-HH-TEST",18);
            parkSystem.park("KA-01-HH-1234",20);
            parkSystem.park("KA-01-HH-T123",18);
            let foundSlots = parkSystem.getVehicleNumbersForAge(20);
            assert.equal(foundSlots.length,1,"Slot Number length 1");
            assert.equal(foundSlots[0],"KA-01-HH-1234","First Element should be 2");
        })
    })


    describe("#getSlotForVehicleNumber()",()=>{
        let parkSystem = new ParkingSystem(10);

        it("get slot for vehicle",()=>{
            parkSystem.park("KA-01-HH-1234",18);
            parkSystem.park("KA-01-HH-1234",20);
            parkSystem.park("KA-01-HH-TEST",18);
            let slot = parkSystem.getSlotForVehicleNumber("KA-01-HH-TEST");
            assert.equal(slot,3,"Check Assigned slot");
        });

        it("it should throw error",()=>{
            try {
                let slot = parkSystem.getSlotForVehicleNumber("KA-01-HH-FFFF");
                assert.fail("it should error throw message");
            }catch (e) {}

        });

    })




});