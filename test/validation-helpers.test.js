const assert = require('assert');
const {isValidVehicleNumber,isValidInt} = require("../utils/validation-helpers");


describe("Validations", ()=> {
    describe("#isValidVehicleNumber()",() => {
        it("it should return true",()=>{
            assert.equal(true,isValidVehicleNumber("KA-01-HH-1234"))
        })
        it("it should return false",()=>{
            assert.equal(false,isValidVehicleNumber("KA-01-HH-123H"))
        })
        it("it should return false",()=>{
            assert.equal(false,isValidVehicleNumber("123456"))
        })
    });

    describe("#isValidInt()",() => {
        it("it should return true",()=>{
            assert.equal(true,isValidInt(123456))
        })
        it("it should return true",()=>{
            assert.equal(true,isValidInt("123"))
        })
        it("it should return false",()=>{
            assert.equal(false,isValidInt("ABC"))
        })
    });




});
