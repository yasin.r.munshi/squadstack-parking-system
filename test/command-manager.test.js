const assert = require('assert');
const {CommandManager} = require("../utils/command-manager");


describe("CommandManager",()=>{
    describe("create Test",()=>{
        let commandManager = new CommandManager();
        it("it should throw creation error",()=>{
            assert.throws(()=>{
                let message = commandManager.processCommand("Park KA-01-HH-1234 driver_age 21");
            },"creation error should be thrown")
        })
        it("it should Create a new Parking System",()=>{
            let message = commandManager.processCommand("Create_parking_lot 3");
            assert.equal(message,"Created parking of 3 slots","Check Response message");
        })

    })

    describe("#processCommand",()=>{
        let commandManager = new CommandManager();
        beforeEach(()=>{
            commandManager = new CommandManager();
            commandManager.processCommand("Create_parking_lot 10");
            commandManager.processCommand("Park KA-01-HH-1234 driver_age 21");
            commandManager.processCommand("Park KA-01-HH-5678 driver_age 34");
            commandManager.processCommand("Park KA-02-HH-5678 driver_age 34");
            commandManager.processCommand("Park KA-03-HH-5678 driver_age 21");
        });


        it("it should throw already created error",()=>{
            assert.throws(()=>{
                let message = commandManager.processCommand("Create_parking_lot 10");
            },"creation error should be thrown")
        })


        it("it should Park the car",()=>{
            let message = commandManager.processCommand("Park KA-01-HH-1234 driver_age 21");
            assert.equal(message,"Car with vehicle registration number \"KA-01-HH-1234\" has been parked at slot number 5","Check Response message");
        })
        it("it should throw invalid Vehicle error ",()=>{
            assert.throws(()=>{
                let message = commandManager.processCommand("Park KA-01-HH-XYZ driver_age 21");
            },"should throw an error")
        })




        it("it should give slot 1,2",()=>{
            let message = commandManager.processCommand("Slot_numbers_for_driver_of_age 21");
            assert.equal(message,"1,4","Check Response message");
        })
        it("it should give error 'No parked car matches the query'",()=>{
            try{
                let message = commandManager.processCommand("Slot_numbers_for_driver_of_age 22");a
                assert.fail(message);
            }catch (e) {
                assert.equal(e.message,"No parked car matches the query","Check Response message");
            }
        })



        it("it should give slot number for KA-01-HH-1234",()=>{
            let message = commandManager.processCommand("Slot_number_for_car_with_number KA-01-HH-1234");
            assert.equal(message,"1","Check Response message");
        })
        it("it should leave the spot 2",()=>{
            let message = commandManager.processCommand("Leave 2");
            assert.equal(message,"Slot number 2 vacated, the car with the vehicle registration number \"KA-01-HH-5678\" left the space, the driver of the car was age of 34","Check Response message");
        })




        it("it should say 'Slot already vacant'",()=>{
            try{
                let message = commandManager.processCommand("Leave 7");
                assert.fail(message);
            }catch (e) {
                assert.equal(e.message,"Slot already vacant","Check Response message");
            }
        })



        it("it should give vehicle number 'KA-01-HH-6666' for age 23",()=>{
            commandManager.processCommand("Park KA-01-HH-6666 driver_age 23");
            let message = commandManager.processCommand("Vehicle_registration_number_for_driver_of_age 23");
            assert.equal(message,"Vehicle registration number(s) KA-01-HH-6666 for age 23","Check Response message");
        })
        it("it should give Query Error",()=>{
            try{
                let message = commandManager.processCommand("Vehicle_registration_number_for_driver_of_age 24");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"No parked car matches the query","Check Response message");

            }
        })


        // Validation checks
        it("it should Validation error for Vehicle for age ",()=>{
            try{
                let message = commandManager.processCommand("Vehicle_registration_number_for_driver_of_age ABC");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"Age is invalid!","Check Response message");

            }
        })
        it("it should Validation error for Park",()=>{
            try{
                let message = commandManager.processCommand("Park KA-01-HH-1234 driver_age test");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"Age is invalid!","Check Response message");

            }
        })
        it("it should Validation error for Vehicle for age ",()=>{
            try{
                let message = commandManager.processCommand("Slot_numbers_for_driver_of_age test");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"Age is invalid!","Check Response message");

            }
        })

        it("it should Validation error for Parameters  ",()=>{
            try{
                let message = commandManager.processCommand("Slot_numbers_for_driver_of_age");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"Missing arguments!","Check Response message");

            }
        })
        it("it should Validation error for Parameters for park  ",()=>{
            try{
                let message = commandManager.processCommand("Park KA-01-HH-1234");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"Missing arguments!","Check Response message");

            }
        })
        it("it should Validation error for Parameters Leave ",()=>{
            try{
                let message = commandManager.processCommand("Leave");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"Missing arguments!","Check Response message");

            }
        })
        it("it should Validation error for Parameters Slot_number_for_car_with_number ",()=>{
            try{
                let message = commandManager.processCommand("Slot_number_for_car_with_number");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"Missing arguments!","Check Response message");

            }
        })



        it("it should Validation error for Parameters Create_parking_lot ",()=>{
            try{
                let message = commandManager.processCommand("Create_parking_lot");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"Please pass the number of available slots as first parameters","Check Response message");

            }
        })
        it("it should Validation error for message Create_parking_lot ",()=>{
            try{
                let message = commandManager.processCommand("Create_parking_lot test");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"Please pass number in as first parameters","Check Response message");

            }
        })


        it("it should Validation error for message Slot_number_for_car_with_number ",()=>{
            try{
                let message = commandManager.processCommand("Slot_number_for_car_with_number test");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"Vehicle number is invalid!","Check Response message");

            }
        })


        it("it should Validation error for message Leave ",()=>{
            try{
                let message = commandManager.processCommand("Slot_number_for_car_with_number test");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"Vehicle number is invalid!","Check Response message");

            }
        })

        it("it should Validation error for message Leave ",()=>{
            try{
                let message = commandManager.processCommand("Leave test");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"Slot number is invalid!","Check Response message");

            }
        })

        it("it should Validation error for message Vehicle_registration_number_for_driver_of_age ",()=>{
            try{
                let message = commandManager.processCommand("Vehicle_registration_number_for_driver_of_age");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"Missing arguments!","Check Response message");

            }
        })
        it("it should Validation error for message Wrong_command  ",()=>{
            try{
                let message = commandManager.processCommand("Wrong_command");
                assert.fail(message);
            }catch (e){
                assert.equal(e.message,"Invalid Command!!","Check Response message");

            }
        })

    });
})