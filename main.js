const {CommandManager} = require("./utils/command-manager");
const {join} = require("path")
const {readFileSync,existsSync} = require("fs");

function main(args){
    let commandManager = new CommandManager();
    if(args.length > 3){
        console.error("Please pass file name as argument");
        return;
    }
    let fullFileName  = join(__dirname,args[2]);
    if(!existsSync(fullFileName)){
        console.error("Invalid File name");
        return;
    }
    let fileData = readFileSync(fullFileName).toString();

    fileData
        .split("\n")
        .forEach(command => {
        try{
            console.log(commandManager.processCommand(command));
        }catch (e) {
            console.log(e.message);
        }
    });

}


main(process.argv);
